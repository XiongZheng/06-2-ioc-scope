package com.twuc.webApp.yourTurn;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class SimplePrototypeScopeClass {

}
