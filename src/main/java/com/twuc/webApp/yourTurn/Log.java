package com.twuc.webApp.yourTurn;

import java.util.ArrayList;

public class Log {
    private static ArrayList<String> log=new ArrayList<>();

    public static ArrayList<String> getLog() {
        return log;
    }
    public static void add(String message){
        log.add(message);
    }
}
