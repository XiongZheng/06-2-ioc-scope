package com.twuc.webApp.yourTurn;

import org.springframework.stereotype.Component;

@Component
public class ChildClass extends FatherClass{

    public ChildClass() {
        log.add("child");
    }
}
