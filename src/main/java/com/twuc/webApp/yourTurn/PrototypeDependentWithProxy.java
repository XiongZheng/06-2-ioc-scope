package com.twuc.webApp.yourTurn;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE,proxyMode = ScopedProxyMode.TARGET_CLASS)
public class PrototypeDependentWithProxy {
    private int count=0;

    public int someMethod(){
        return count++;
    }
    public PrototypeDependentWithProxy() {
        Log.add("prototype");
    }
}
