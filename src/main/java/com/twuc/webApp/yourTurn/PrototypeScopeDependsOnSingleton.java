package com.twuc.webApp.yourTurn;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class PrototypeScopeDependsOnSingleton {
    private SingletonDependent singletonDependent;

    public SingletonDependent getSingletonDependent() {
        return singletonDependent;
    }

    public PrototypeScopeDependsOnSingleton(SingletonDependent singletonDependent) {
        this.singletonDependent = singletonDependent;
    }
}
