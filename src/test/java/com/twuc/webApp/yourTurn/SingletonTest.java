package com.twuc.webApp.yourTurn;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.util.ClassUtils;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class SingletonTest {

    private AnnotationConfigApplicationContext context;

    @BeforeEach
    void setUp() {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp.yourTurn");
        Log.add("container");
    }

    //2.1-1
    @Test
    void should_test_same_bean() {
        InterfaceOneImpl interfaceOneImpl = context.getBean(InterfaceOneImpl.class);
        InterfaceOne interfaceOne = context.getBean(InterfaceOne.class);
        assertSame(interfaceOneImpl, interfaceOne);
    }

    //2.1-2
    @Test
    void should_test_same_bean_with_extend() {
        ChildClass childClass = context.getBean(ChildClass.class);
        FatherClass fatherClass = context.getBean(FatherClass.class);
        assertSame(childClass, fatherClass);
    }

    @Test
    void should_get_one_child_message() {
        FatherClass fatherClass = context.getBean(FatherClass.class);
        assertEquals(ChildClass.class, fatherClass.getClass());
    }

    //2.1-3
    @Test
    void should_sure_same_object_between_abstract_extends() {
        AbstractBaseClass abstractBaseClass = context.getBean(AbstractBaseClass.class);
        DerivedClass derivedClass = context.getBean(DerivedClass.class);
        assertSame(abstractBaseClass, derivedClass);
    }

    //2.2-1
    @Test
    void should_sure_two_object_when_create_multiple_prototype() {
        SimplePrototypeScopeClass prototype = context.getBean(SimplePrototypeScopeClass.class);
        SimplePrototypeScopeClass prototype2 = context.getBean(SimplePrototypeScopeClass.class);
        assertNotSame(prototype, prototype2);
    }

    //2.2-2
    @Test
    void should_get_singleton_prototype_container_create_order() {
        SingletonDependsOnPrototypeProxyBatchCall singleton = context.getBean(SingletonDependsOnPrototypeProxyBatchCall.class);
        singleton.getPrototypeDependentWithProxy().someMethod();
    }

    //2.2-3
    @Test
    void should_get_double_object_use_double_context() {
        SimpleSingletonClass singleton = context.getBean(SimpleSingletonClass.class);
        AnnotationConfigApplicationContext context2 = new AnnotationConfigApplicationContext("com.twuc.webApp.yourTurn");
        SimpleSingletonClass singleton2 = context2.getBean(SimpleSingletonClass.class);
        assertNotSame(singleton, singleton2);
    }

    //2.2-4
    @Test
    void should_get_two_prototype_and_singleton_when_prototype_depend_singleton() {
        PrototypeScopeDependsOnSingleton prototype = context.getBean(PrototypeScopeDependsOnSingleton.class);
        PrototypeScopeDependsOnSingleton prototype2 = context.getBean(PrototypeScopeDependsOnSingleton.class);
        assertNotSame(prototype, prototype2);
        assertSame(prototype.getSingletonDependent(),prototype2.getSingletonDependent());
    }

    //2.2-5
    @Test
    void should_get_one_prototype_and_one_singleton_when_singleton_depend_prototype() {
        SingletonDependsOnPrototype singleton = context.getBean(SingletonDependsOnPrototype.class);
        SingletonDependsOnPrototype singleton2 = context.getBean(SingletonDependsOnPrototype.class);
        assertSame(singleton,singleton2);
    }

    //2.3-1
    @Test
    void should_create_two_prototype_proxy() {
        SingletonDependsOnPrototypeProxy singleton = context.getBean(SingletonDependsOnPrototypeProxy.class);
        SingletonDependsOnPrototypeProxy singleton2 = context.getBean(SingletonDependsOnPrototypeProxy.class);
        int count1 = singleton.getPrototypeDependentWithProxy().someMethod();
        int count2 = singleton2.getPrototypeDependentWithProxy().someMethod();
        assertEquals(count1,count2);
    }

    //2.3-2
    @Test
    void should_create_two_prototype_call_singleton_twice() {
        SingletonDependsOnPrototypeProxyBatchCall singleton = context.getBean(SingletonDependsOnPrototypeProxyBatchCall.class);
        String s = singleton.getPrototypeDependentWithProxy().toString();
        String s2 = singleton.getPrototypeDependentWithProxy().toString();
        assertNotEquals(s,s2);
    }

    //2.3-3
    @Test
    void should_get_same_proxy() {
        SingletonDependsOnPrototypeProxy singleton = context.getBean(SingletonDependsOnPrototypeProxy.class);
        assertTrue(ClassUtils.isCglibProxyClass(singleton.getPrototypeDependentWithProxy().getClass()));
    }
}
